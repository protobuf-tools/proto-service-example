package org.silbertb.proto.example.shape.area.shapes;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@ProtoClass(protoClass = org.silbertb.proto.example.shape.Rectangle.class)
public class Rectangle implements Shape{

    private final double a;
    private final double b;

    @ProtoConstructor
    public Rectangle(@ProtoField(protoName = "width") double a, @ProtoField(protoName = "length") double b) {
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }


    @Override
    public double calculateArea() {
        return a * b;
    }
}
