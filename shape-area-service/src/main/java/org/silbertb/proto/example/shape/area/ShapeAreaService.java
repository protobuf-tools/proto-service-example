package org.silbertb.proto.example.shape.area;

import io.grpc.stub.StreamObserver;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.example.shape.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShapeAreaService extends AreaServiceGrpc.AreaServiceImplBase {

    private Logger logger = LoggerFactory.getLogger(ShapeAreaService.class);

    @Override
    public void calculateArea(CalculateAreaRequest request, StreamObserver<CalculateAreaResponse> responseObserver) {
        Shape shape = request.getShape();
        org.silbertb.proto.example.shape.area.shapes.Shape domainShape = ProtoDomainConverter.toDomain(shape);

        double area = domainShape.calculateArea();

        CalculateAreaResponse response = CalculateAreaResponse.newBuilder().setArea(area).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
