package org.silbertb.proto.example.shape.area;

import io.envoyproxy.pgv.ReflectiveValidatorIndex;
import io.envoyproxy.pgv.ValidatorIndex;
import io.envoyproxy.pgv.grpc.ValidatingServerInterceptor;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerInterceptors;
import io.grpc.ServerServiceDefinition;
import io.grpc.protobuf.services.ProtoReflectionService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {
        int port = 50051;
        for(int i=0; i<args.length; i++) {
            switch (args[i]) {
                case "-p":
                    i++;
                    port = Integer.parseInt(args[i]);
                    break;
                default:
                    printHelp();
                    System.exit(1);
            }
        }

        ValidatorIndex index = new ReflectiveValidatorIndex();
        ShapeAreaService shapeAreaService = new ShapeAreaService();
        ServerServiceDefinition validatorInterceptor = ServerInterceptors.intercept(shapeAreaService, new ValidatingServerInterceptor(index));
        Server grpcServer = ServerBuilder
                .forPort(port)
                .addService(shapeAreaService)
                .addService(validatorInterceptor)
                .addService(ProtoReflectionService.newInstance())
                .build();

        grpcServer.start();

        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                System.err.println("*** shutting down gRPC server ***");
                try {
                    grpcServer.shutdown().awaitTermination(30, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace(System.err);
                }
                System.err.println("*** Server shut down ***");
            }
        });

        grpcServer.awaitTermination();
        System.out.println("Done");
    }

    private static void printHelp() {
        System.err.println("Usage: areaService [-p <port>]");
    }
}
