package org.silbertb.proto.example.shape.area.shapes;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@ProtoClass(protoClass = org.silbertb.proto.example.shape.Triangle.class)
public class Triangle implements Shape{

    private double a;
    private double b;
    private double c;

    @ProtoConstructor
    public Triangle(@ProtoField(protoName = "side_a") double a, @ProtoField(protoName = "side_b") double b, @ProtoField(protoName = "side_c") double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    @Override
    public double calculateArea() {
        double p = (a + b + c) / 2.0;
        return Math.sqrt(p*(p-a)*(p-b)*(p-c));
    }
}
