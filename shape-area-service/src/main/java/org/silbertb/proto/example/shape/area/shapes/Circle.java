package org.silbertb.proto.example.shape.area.shapes;


import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@ProtoClass(protoClass = org.silbertb.proto.example.shape.Circle.class)
public class Circle implements Shape{
    private final double radius;

    @ProtoConstructor
    public Circle(@ProtoField double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double calculateArea() {
        return Math.PI * radius * radius;
    }
}
