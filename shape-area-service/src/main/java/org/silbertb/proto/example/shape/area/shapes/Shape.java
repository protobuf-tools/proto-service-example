package org.silbertb.proto.example.shape.area.shapes;

import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;

@OneofBase(oneofName = "shape", oneOfFields = {
        @OneofField(protoField = "circle", domainClass = Circle.class),
        @OneofField(protoField = "rectangle", domainClass = Rectangle.class),
        @OneofField(protoField = "triangle", domainClass = Triangle.class)
})
@ProtoClass(protoClass = org.silbertb.proto.example.shape.Shape.class)
public interface Shape {
    double calculateArea();
}
